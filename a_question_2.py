"""
# tau = t \c dot Omega_r

use this for 3d plots:
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    to make 3d
    ax.scatter(xs, ys, zs, marker='x', c=t, cmap='jet')

"""
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from numba import jit
from utilities import my_plotting_style as mps
from utilities import my_wrappers as mwr
mps.set_defaults()


def run_first_example():
    """
    A simple function to demonstrate how scipy.integrate.odeint works
    Stolen from a youtube video.

    :return: void
    """

    @jit(nopython=True)
    def firstorder(y, t, K, u, tau):
        """

        :param y: the value at that point in time
        :param t: the time [parameter for the integration algorithm]
        :param K: the responsiveness
        :param u: the forcing
        :param tau: the time constant of response
        :return: dydt - the first derivative for the integration algorithm
        """
        dydt = (-y + K * u) / tau
        return dydt

    t = np.linspace(0, 10, 20)
    K = 2.0
    tau = 5.0
    # u = 1.0
    u = np.zeros(len(t))
    u[3:] = 1
    y0 = 0
    y1 = np.zeros(len(t))

    for i in range(len(t)-1):
        ts = [t[i], t[i+1]]
        y = odeint(firstorder, y0, ts, args=(K, u[i], tau))
        y0 = y[1]
        print(y[1])
        y1[i+1] = y0

    plt.plot(t, y1)
    plt.xlabel('time (s)')
    plt.ylabel('y value')
    plt.show()


# run_first_example()


def run_bloch_equations(bloch_v_init=[0, 0, 1],
                        decay_rate_drr=0,
                        detuning_drr=0,
                        rabi_freq_drr=0,
                        figure_name='decaying'):
    """

    :param bloch_v_init: bloch vector initial position
    :param decay_rate_drr:
    :param detuning_drr:
    :param rabi_freq_drr:
    :param figure_name:
    :return:
    """
    @jit(nopython=True)
    def firstorder(bloch_v, t, detuning_drr, decay_rate_drr, rabi_freq_drr):
        """

        :param bloch_v:
        :param t:
        :param detuning_drr:
        :param decay_rate_drr:
        :param rabi_freq_drr:
        :return:
        """
        dydtau = [detuning_drr*bloch_v[1] - 0.5*decay_rate_drr*bloch_v[0],
                  -detuning_drr*bloch_v[0] + rabi_freq_drr*bloch_v[2] - 0.5*decay_rate_drr*bloch_v[1],
                  -rabi_freq_drr*bloch_v[1] - decay_rate_drr*(bloch_v[2] - 1)]
        return dydtau

    t = np.linspace(0, 1000, 10000)

    @mwr.timeit
    def run_integration(t, bloch_v_init, detuning_drr, decay_rate_drr, rabi_freq_drr):
        """

        :param t: time vector
        :param bloch_v_init:
        :param detuning_drr:
        :param decay_rate_drr:
        :param rabi_freq_drr:
        :return:
        """
        bloch_v_storage = np.zeros([len(t), 3])
        bloch_v_storage[0] = bloch_v_init
        for i in range(len(t)-1):
            ts = [t[i], t[i+1]]
            y = odeint(firstorder, bloch_v_init, ts, args=(detuning_drr, decay_rate_drr,
                                                           rabi_freq_drr))
            bloch_v_init = y[1]
            bloch_v_storage[i+1, :] = y[1]
        return bloch_v_storage

    bloch_v = run_integration(t, bloch_v_init, detuning_drr, decay_rate_drr, rabi_freq_drr)

    @mwr.timeit
    def plot_bloch_vectors(t, bloch_v, figure_name='decaying'):
        """

        :param t:
        :param bloch_v:
        :return:
        """
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(bloch_v[:, 0], bloch_v[:, 1], bloch_v[:, 2], marker='x', c=t, cmap='jet')
        ax.set_xlabel('u')
        ax.set_ylabel('v')
        ax.set_zlabel('w')
        ax.view_init(azim=-40, elev=35)

        u, v = np.mgrid[0:2 * np.pi:50j, 0:np.pi:50j]
        x = np.cos(u) * np.sin(v)
        y = np.sin(u) * np.sin(v)
        z = np.cos(v)
        ax.plot_wireframe(x, y, z, color="k", alpha=0.2, linewidth=0.5)
        fig = mps.defined_size(fig, size='dcsq')
        # plt.show()
        plt.savefig('plots/a_rabi_' + figure_name + '.pdf')

    plot_bloch_vectors(t, bloch_v, figure_name=figure_name)


# run_bloch_equations(bloch_v_init=[0, 0, 1], rabi_freq_drr=1, detuning_drr=1, omega=1)

run_bloch_equations(bloch_v_init=[0, 0, 1],
                    rabi_freq_drr=1,
                    detuning_drr=1,
                    decay_rate_drr=0.01,
                    figure_name='decaying')

run_bloch_equations(bloch_v_init=[0, 0, 1],
                    rabi_freq_drr=1,
                    detuning_drr=0,
                    decay_rate_drr=0.01,
                    figure_name='not_detuned_decaying')

run_bloch_equations(bloch_v_init=[0, 0, 1],
                    rabi_freq_drr=1,
                    detuning_drr=0,
                    decay_rate_drr=0,
                    figure_name='not_detuned')

run_bloch_equations(bloch_v_init=[1/np.sqrt(2), 0, 1/np.sqrt(2)],
                    rabi_freq_drr=1,
                    detuning_drr=0.1,
                    decay_rate_drr=0.1,
                    figure_name='different_start')
