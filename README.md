# aop

*Atomic and Optical Physics*

_Optical Bloch equations_

The aim of this exercise is to obtain a quantitative understanding of the optical Bloch equations including dissipation. For
this purpose the differential equations will be solved numerically. You are free in the choice of the programming language
(Mathematica, Maple, C).
It is advisable to use a scaled version of the Bloch equation by substituting τ := t · ΩR. Therefore, the length of one
Rabi-Oscillation period on resonance is τ0 = 2π.

(a) List the scaled Bloch equations in the vector description (u,v,w).

(b) Optical Bloch equations without damping
Solve the optical Bloch equations without damping (Γ = 0) for the following cases numerically (and for time dependent
∆(t)). Plot the path that the tip of the Bloch vectors follows (in a 3D plot) for each case. Always start out with the atom
in the ground state.
- Show that for ∆ = 0 Rabi oscillations occur. Around which axis does the Bloch vector rotate?
- Consider the cases ∆ = Ω R und ∆ = 10 Ω R. Use the analytic Bloch equations to explain how the oscillation frequency
and the rotation axis change.
- Adiabatic transfer: The detuning is changed linearly from −10 ΩR to 10 ΩR in the time interval ∆τ = 2π × 100.
- Nonadiabatic transfer: The detuning is changed linearly from −10 ΩR to 10 ΩR in the time interval ∆τ = 2π × 5.

Describe the difference between adiabatic and nonadiabatic transfer.

(c) Optical Bloch equations with damping
How does the Bloch vector evolve for ∆ = ΩR and Γ = 0.1 ΩR? Compare with the undamped system (see above).
Calculate the length of the Bloch vector L(t) for the above case. Try to comment on the general qualitative behaviour:
What effect does the damping have on the Bloch vector?

Hints on the programming in Mathematica

Differential equations can be solved numerically in Mathematica with the command NDSolve, whose complete options
can be investigated in the Mathematica-Help.
3D-Plots can be generated with the commands Plot3D or better with ParametricPlot3D.
